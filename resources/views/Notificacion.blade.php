@extends('layouts.app')

@section('content')
<style>

</style>
<!-- will be used to show any messages -->
@if (Session::has('mensaje'))
    <div class="alert alert-success" role="alert">
    {{ Session::get('mensaje') }}
</div>

@endif


    <div class="tabla">
    <div class="float-right">
      
        <a class="btn btn-danger btn-large" href="{{route('nuevaNotificacion')}}">Nueva Notificacion</a>

    </div><br>
    <br><br>
    <div class="row col">
        <div  >
            
            
        
        
        <table width="70%"  class="table table-responsive">
        <thead>
            <tr height="50">
            <th scope="col">Fecha</th>
            <th scope="col">Nombre</th>
            <th scope="col">Imagen</th>
            <th scope="col">Contacto</th>
            <th class="tablita">Descripcion</th>
            <th scope="col">Link</th>


            <th scope="col"></th>

            </tr>
        </thead>
        <tbody>
        @foreach ($notificaciones as $notificacion)
            <tr>
                <td> {{ $notificacion->created_at }}</td>
                <td> {{ $notificacion->titulo }}</td>
                <td>   
                
               
                <a href='{{URL::asset("/app/public/".$notificacion->imagen)}}' data-toggle="lightbox" data-max-width="1050">
                        <img  style="height:50px;" src='{{URL::asset("/app/public/".$notificacion->imagen)}}' class="img-fluid">
                    </a>
                 </td>


               
                <td> {{ $notificacion->contacto }}</td>
                <td class="tablita"> {{ $notificacion->descripcion }}</td>
                <td>  <a href="{{ $notificacion->link }} " target="_blank">Link</a></td>

                <td width="300" style="width:300px;">  
                <a class="btn btn-primary btn-large" href="{{route('notificacion.get' , ['id' => $notificacion->id] )}}">Información</a>

            
                 </td>

               
            </tr>
         @endforeach

        </tbody>
    </table>
     {{ $notificaciones->links() }}
        </div>

    </div>
    
    </div>

<script>
$(document).on('click', '[data-toggle="lightbox"]', function(event) {
                event.preventDefault();
                $(this).ekkoLightbox();
            });
</script>
<style>

.tablita{
           width:  500px  !important;

}
.tabla{
           width:  70%  !important;

}
</style>
@endsection