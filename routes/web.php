<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'NotificacionController@index')->name('notificacion');
Route::get('/nueva_notificacion', 'NotificacionController@create')->name('nuevaNotificacion');
Route::post('/notificacion', 'NotificacionController@store')->name('save_notificacion');
Route::get('/notificacion/{id}', 'NotificacionController@detalle')->name('notificacion.get');

Route::get('/mandarwha', 'NotificacionController@mandarwha')->name('notificacion.mandarwha');
Route::post('/whatsandbox', 'NotificacionController@whatsandbox')->name('notificacion.whatSandbox');




//Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
